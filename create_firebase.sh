#!/bin/bash

# Ask the user for options

read -p "Enter flutter project name: " projectName



# Run the actual flutter create command
# Convert project name to lowercase
projectNameLowerCase=$(echo "$projectName" | tr '[:upper:]' '[:lower:]')

flutter create $projectNameLowerCase


# Navigate to the created project directory
cd $projectNameLowerCase


# Firebase

read -p "Do you want to use Firebase? (y/n): " useFirebase
if [[ $useFirebase == [Yy] ]]; then
  
        # Install Firebase CLI
    export PATH="$PATH":"$HOME/.pub-cache/bin"

    dart pub global activate flutterfire_cli
        # Configure Firebase
    flutterfire configure

fi

# State management

read -p "Enter 1 for Bloc or 2 for Riverpod: " stateManagement

if [[ $stateManagement == [1] ]]; then
    echo "Adding flutter_bloc..."
    flutter pub add flutter_bloc
    flutter pub get
fi

if [[ $stateManagement == [2] ]]; then
    echo "Adding riverpod..."
    flutter pub add riverpod
    flutter pub get
fi


#Clean code architecture
read -p "Do you want to add clean code architecture? (y/n): " useCleanCode

if [[ $useCleanCode == [Yy] ]]; then
    mkdir -p lib/data/temp_feature/data_source lib/data/temp_feature/repository lib/data/temp_feature/models
    mkdir -p lib/domain/temp_feature/entity lib/domain/temp_feature/repository lib/domain/temp_feature/use_case
    mkdir -p lib/presentation/temp_feature/screens lib/presentation/temp_feature/widgets

    if [[ $stateManagement == 1 ]]; then
        mkdir -p lib/presentation/temp_feature/temp_bloc/events lib/presentation/temp_feature/temp_bloc/states lib/presentation/temp_feature/temp_bloc/bloc
    fi
    if [[ $stateManagement == 2 ]]; then
        mkdir -p lib/presentation/temp_feature/riverpod
    fi
fi

# Activating mason

read -p "Do you want to add other files? (y/n)" useMason

if[[useMason == [Yy]]]; then
   export PATH="$PATH":"$HOME/.pub-cache/bin"
   dart pub global activate mason_cli
fi
   


echo "Project setup completed!"
